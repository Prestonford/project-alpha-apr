from django.shortcuts import render, redirect
from projects.models import Project, ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "list_project": list,
    }
    return render(request, "projects/projects.html", context)


# feature 13
@login_required
def project_details(request, id):
    project = Project.objects.get(id=id)
    print(project)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
